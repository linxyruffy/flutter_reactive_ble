## 4.1.0

* Add support Android 12

## 4.0.1

* Add support for iOS 15

## 4.0.0

* Initial Open Source release.

